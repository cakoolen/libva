package vaapi

/*
#include <va/va.h>
*/
import "C"
import (
	"fmt"
)

type VAError C.VAStatus

const (
	ErrOperationFailed        VAError = C.VA_STATUS_ERROR_OPERATION_FAILED
	ErrAllocationFailed       VAError = C.VA_STATUS_ERROR_ALLOCATION_FAILED
	ErrInvalidDisplay         VAError = C.VA_STATUS_ERROR_INVALID_DISPLAY
	ErrInvalidConfig          VAError = C.VA_STATUS_ERROR_INVALID_CONFIG
	ErrInvalidContext         VAError = C.VA_STATUS_ERROR_INVALID_CONTEXT
	ErrInvalidSurface         VAError = C.VA_STATUS_ERROR_INVALID_SURFACE
	ErrInvalidBuffer          VAError = C.VA_STATUS_ERROR_INVALID_BUFFER
	ErrInvalidImage           VAError = C.VA_STATUS_ERROR_INVALID_IMAGE
	ErrInvalidSubpicture      VAError = C.VA_STATUS_ERROR_INVALID_SUBPICTURE
	ErrAttrNotSupported       VAError = C.VA_STATUS_ERROR_ATTR_NOT_SUPPORTED
	ErrMaxNumExceeded         VAError = C.VA_STATUS_ERROR_MAX_NUM_EXCEEDED
	ErrUnsupportedProfile     VAError = C.VA_STATUS_ERROR_UNSUPPORTED_PROFILE
	ErrUnsupportedEntrypoint  VAError = C.VA_STATUS_ERROR_UNSUPPORTED_ENTRYPOINT
	ErrUnsupportedRtFormat    VAError = C.VA_STATUS_ERROR_UNSUPPORTED_RT_FORMAT
	ErrUnsupportedBuffertype  VAError = C.VA_STATUS_ERROR_UNSUPPORTED_BUFFERTYPE
	ErrSurfaceBusy            VAError = C.VA_STATUS_ERROR_SURFACE_BUSY
	ErrFlagNotSupported       VAError = C.VA_STATUS_ERROR_FLAG_NOT_SUPPORTED
	ErrInvalidParameter       VAError = C.VA_STATUS_ERROR_INVALID_PARAMETER
	ErrResolutionNotSupported VAError = C.VA_STATUS_ERROR_RESOLUTION_NOT_SUPPORTED
	ErrUnimplemented          VAError = C.VA_STATUS_ERROR_UNIMPLEMENTED
	ErrSurfaceInDisplaying    VAError = C.VA_STATUS_ERROR_SURFACE_IN_DISPLAYING
	ErrInvalidImageFormat     VAError = C.VA_STATUS_ERROR_INVALID_IMAGE_FORMAT
	ErrDecodingError          VAError = C.VA_STATUS_ERROR_DECODING_ERROR
	ErrEncodingError          VAError = C.VA_STATUS_ERROR_ENCODING_ERROR
)

var errorMap = map[VAError]string{
	ErrOperationFailed:        "operation failed",
	ErrAllocationFailed:       "allocation failed",
	ErrInvalidDisplay:         "invalid display",
	ErrInvalidConfig:          "invalid config",
	ErrInvalidContext:         "invalid context",
	ErrInvalidSurface:         "invalid surface",
	ErrInvalidBuffer:          "invalid buffer",
	ErrInvalidImage:           "invalid image",
	ErrInvalidSubpicture:      "invalid subpicture",
	ErrAttrNotSupported:       "attr not supported",
	ErrMaxNumExceeded:         "max num exceeded",
	ErrUnsupportedProfile:     "unsupported profile",
	ErrUnsupportedEntrypoint:  "unsupported entrypoint",
	ErrUnsupportedRtFormat:    "unsupported rt format",
	ErrUnsupportedBuffertype:  "unsupported buffertype",
	ErrSurfaceBusy:            "surface busy",
	ErrFlagNotSupported:       "flag not supported",
	ErrInvalidParameter:       "invalid parameter",
	ErrResolutionNotSupported: "resolution not supported",
	ErrUnimplemented:          "unimplemented",
	ErrSurfaceInDisplaying:    "surface in displaying",
	ErrInvalidImageFormat:     "invalid image format",
	ErrDecodingError:          "decoding error",
	ErrEncodingError:          "encoding error",
}

func (s VAError) Error() string {
	if s, ok := errorMap[s]; ok {
		return s
	}
	return fmt.Sprintf("unknown error %v", C.VAStatus(s))
}
