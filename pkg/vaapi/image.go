package vaapi

/*
#include <va/va.h>
*/
import "C"
import "image"

type Image struct {
	id C.VAImage
	d  *Device
}

func (i *Image) Close() error {
	if vaStatus := C.vaDestroyImage(i.d.d, i.id.image_id); vaStatus != C.VA_STATUS_SUCCESS {
		return VAError(vaStatus)
	}

	return nil
}

func (i *Image) UploadYVbCr(img *image.YCbCr) error {
	buffer, err := i.d.MapBuffer(i.id.buf, i.id.data_size)
	if err != nil {
		return err
	}

	defer i.d.UnmapBuffer(i.id.buf)

	y_dst := buffer[i.id.offsets[0]:]
	y_src := img.Y

	u_dst := buffer[i.id.offsets[1]:]
	u_src := img.Cb
	v_src := img.Cr

	for row := 0; row < int(i.id.height); row++ {
		copy(y_dst, y_src[:img.YStride])
		y_dst = y_dst[i.id.pitches[0]:]
		y_src = y_src[img.YStride:]
	}

	for row := 0; row < int(i.id.height/2); row++ {
		for col := 0; col < int(i.id.width/2); col++ {
			u_dst[col*2] = u_src[col]
			u_dst[col*2+1] = v_src[col]
		}
		u_dst = u_dst[i.id.pitches[1]:]
		u_src = u_src[img.CStride:]
		v_src = v_src[img.CStride:]
	}

	return nil
}
