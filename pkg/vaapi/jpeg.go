package vaapi

/*
#cgo CFLAGS: -DHAVE_CONFIG_H -I/usr/include/libdrm
#cgo LDFLAGS:  -ldrm -lva-drm -lva

#include <va/va.h>
#include <va/va_enc_jpeg.h>

void setVAConfigAttribValEncJPEGValue(
	VAConfigAttribValEncJPEG *a,
	uint32_t value) {
	a->value = value;
}

void setVAConfigAttribValEncJPEGArithmaticCodingMode(
	VAConfigAttribValEncJPEG *a,
	uint32_t mode) {
	a->bits.arithmatic_coding_mode=mode;
}

void setVAConfigAttribValEncJPEGProgressiveDctMode(
	VAConfigAttribValEncJPEG *a,
	uint32_t mode) {
	a->bits.progressive_dct_mode=mode;
}

void setVAConfigAttribValEncJPEGNonInterleavedMode(
	VAConfigAttribValEncJPEG *a,
	uint32_t mode) {
	a->bits.non_interleaved_mode=mode;
}

void setVAConfigAttribValEncJPEGDifferentialMode(
	VAConfigAttribValEncJPEG *a,
	uint32_t mode) {
	a->bits.differential_mode=mode;
}

uint32_t getVAConfigAttribValEncJPEGValue(
	VAConfigAttribValEncJPEG *a
	) {
	return a->value;
}

void setVAEncPictureParameterBufferJPEGProfile(
	VAEncPictureParameterBufferJPEG *a,
	uint32_t profile){
	a->pic_flags.bits.profile=profile;
}

void setVAEncPictureParameterBufferJPEGProgressive(
	VAEncPictureParameterBufferJPEG *a,
	uint32_t progressive){
	a->pic_flags.bits.progressive=progressive;
}

void setVAEncPictureParameterBufferJPEGHuffman(
	VAEncPictureParameterBufferJPEG *a,
	uint32_t huffman){
	a->pic_flags.bits.huffman=huffman;
}

void setVAEncPictureParameterBufferJPEGInterleaved(
	VAEncPictureParameterBufferJPEG *a,
	uint32_t interleaved){
	a->pic_flags.bits.interleaved=interleaved;
}

void setVAEncPictureParameterBufferJPEGDifferential(
	VAEncPictureParameterBufferJPEG *a,
	uint32_t differential){
	a->pic_flags.bits.differential=differential;
}

*/
import "C"
import (
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"
	"image"
	"image/jpeg"
	"io"
	"unsafe"
)

type yuvComponentSpecs struct {
	yuvType         image.YCbCrSubsampleRatio
	vaSurfaceFormat uint
	fourccVal       uint

	nComponents uint

	yHSubSample uint
	yVSubsample uint

	uHSubSample uint
	uVSubsample uint

	vHSubSample uint
	vVSubsample uint
}

func newVASurfaceAttrib(yuvType image.YCbCrSubsampleRatio) C.VASurfaceAttrib {
	fourcc := C.VASurfaceAttrib{
		_type: C.VASurfaceAttribPixelFormat,
		flags: C.VA_SURFACE_ATTRIB_SETTABLE,
	}
	fourcc.value._type = C.VAGenericValueTypeInteger

	// Set the content of a C union through an unsafe pointer construction
	valuePointer := (*C.int32_t)(unsafe.Pointer(&fourcc.value.value))

	switch yuvType {
	case image.YCbCrSubsampleRatio420:
		*valuePointer = C.VA_FOURCC_NV12
	}

	return fourcc
}

func newYuvComponentSpecs(yuvType image.YCbCrSubsampleRatio) *yuvComponentSpecs {
	c := &yuvComponentSpecs{
		yuvType: yuvType,
	}
	switch yuvType {
	case image.YCbCrSubsampleRatio420:
		c.vaSurfaceFormat = C.VA_RT_FORMAT_YUV420
		c.fourccVal = C.VA_FOURCC_I420
		c.nComponents = 3
		c.yHSubSample = 2
		c.yVSubsample = 2
		c.uHSubSample = 1
		c.uVSubsample = 1
		c.vHSubSample = 1
		c.vVSubsample = 1
	}
	return c
}

func newEncPictureParameters(id C.VABufferID, i *image.YCbCr, quality int) C.VAEncPictureParameterBufferJPEG {
	yuvComponent := newYuvComponentSpecs(i.SubsampleRatio)

	result := C.VAEncPictureParameterBufferJPEG{
		coded_buf:                id,
		picture_width:            C.uint16_t(i.Rect.Dx()),
		picture_height:           C.uint16_t(i.Rect.Dy()),
		quality:                  C.uint8_t(quality),
		sample_bit_depth:         8,
		num_scan:                 1,
		num_components:           C.uint16_t(yuvComponent.nComponents),
		component_id:             [4]C.uint8_t{0, 1, 2, 0},
		quantiser_table_selector: [4]C.uint8_t{0, 1, 1, 0},
	}

	C.setVAEncPictureParameterBufferJPEGProfile(&result, 0)
	C.setVAEncPictureParameterBufferJPEGProgressive(&result, 0)
	C.setVAEncPictureParameterBufferJPEGHuffman(&result, 1)
	C.setVAEncPictureParameterBufferJPEGInterleaved(&result, 0)
	C.setVAEncPictureParameterBufferJPEGDifferential(&result, 0)
	return result

}

func newQMatrix(i *image.YCbCr) C.VAQMatrixBufferJPEG {
	result := C.VAQMatrixBufferJPEG{
		load_lum_quantiser_matrix: 1,
	}

	result.load_chroma_quantiser_matrix = 1

	for i := range result.lum_quantiser_matrix {
		result.lum_quantiser_matrix[i] = C.uint8_t(jpegLumaQuant[jpegZigzag[i]])
		result.chroma_quantiser_matrix[i] = C.uint8_t(jpegChromaQuant[jpegZigzag[i]])
	}

	return result
}

func newHuffmanTable() C.VAHuffmanTableBufferJPEGBaseline {
	result := C.VAHuffmanTableBufferJPEGBaseline{
		load_huffman_table: [2]C.uint8_t{1, 1},
	}

	copy(result.huffman_table[0].num_dc_codes[:], jpegHufftableLumaDc[1:17])
	copy(result.huffman_table[0].dc_values[:], jpegHufftableLumaDc[17:29])
	copy(result.huffman_table[0].num_ac_codes[:], jpegHufftableLumaAc[1:17])
	copy(result.huffman_table[0].ac_values[:], jpegHufftableLumaAc[17:179])
	copy(result.huffman_table[0].pad[:], []C.uint8_t{0, 0})

	copy(result.huffman_table[1].num_dc_codes[:], jpegHufftableChromaDc[1:17])
	copy(result.huffman_table[1].dc_values[:], jpegHufftableChromaDc[17:29])
	copy(result.huffman_table[1].num_ac_codes[:], jpegHufftableChromaAc[1:17])
	copy(result.huffman_table[1].ac_values[:], jpegHufftableChromaAc[17:179])
	copy(result.huffman_table[1].pad[:], []C.uint8_t{0, 0})

	return result
}

func newSliceParameter(nComponents uint) C.VAEncSliceParameterBufferJPEG {
	result := C.VAEncSliceParameterBufferJPEG{
		restart_interval: 0,
		num_components:   C.uint16_t(nComponents),
	}

	result.components[0].component_selector = 1
	result.components[0].dc_table_selector = 0
	result.components[0].ac_table_selector = 0

	result.components[1].component_selector = 2
	result.components[1].dc_table_selector = 1
	result.components[1].ac_table_selector = 1

	result.components[2].component_selector = 3
	result.components[2].dc_table_selector = 1
	result.components[2].ac_table_selector = 1
	return result
}

type quantType int

const (
	lumaSection quantType = iota
	chromaSection
)

func newQuantData(q quantType, quality int) jpegQuantSection {
	s := jpegQuantSection{
		DQT:  dqt,
		Lq:   3 + 64,
		PqTq: uint8(q),
	}

	for i := range s.Qk {
		var temp uint32

		switch q {
		case lumaSection:
			temp = uint32(jpegLumaQuant[jpegZigzag[i]])
		case chromaSection:
			temp = uint32(jpegChromaQuant[jpegZigzag[i]])
		}

		// Scale the quantization table with quality factor
		temp = temp * uint32(quality) / 100

		// Clamp to range [1,255]
		if temp > 255 {
			temp = 255
		}
		if temp < 1 {
			temp = 1
		}
		s.Qk[i] = uint8(temp)
	}

	return s
}

func newFrameHeader(i *image.YCbCr) jpegFrameHeader {
	yuvComponent := newYuvComponentSpecs(i.SubsampleRatio)

	result := jpegFrameHeader{
		SOF:       sof0,
		Lf:        8 + (3 * uint16(yuvComponent.nComponents)),
		P:         8,
		Y:         uint16(i.Rect.Dy()),
		X:         uint16(i.Rect.Dx()),
		Nf:        uint8(yuvComponent.nComponents),
		Component: make([]jpegFrameHeaderComponent, yuvComponent.nComponents),
	}

	for i := range result.Component {
		result.Component[i].Ci = uint8(i + 1)
		if i == 0 {
			result.Component[i].SetHi(uint8(yuvComponent.yHSubSample))
			result.Component[i].SetVi(uint8(yuvComponent.yVSubsample))
			result.Component[i].Tqi = 0
		} else {
			result.Component[i].SetHi(uint8(yuvComponent.uHSubSample))
			result.Component[i].SetVi(uint8(yuvComponent.uVSubsample))
			result.Component[i].Tqi = 1
		}
	}

	return result
}

func newHuffSectionHdr(th, tc int) jpegHuffSection {
	result := jpegHuffSection{
		DHT: dht,
	}

	result.SetTc(uint8(tc))
	result.SetTh(uint8(th))

	result.Li = make([]C.uint8_t, 16)
	if tc == 1 {
		result.Vij = make([]C.uint8_t, 162)
	} else {
		result.Vij = make([]C.uint8_t, 12)
	}

	result.Lh = uint16(3 + len(result.Li) + len(result.Vij))

	if th == 0 {
		if tc == 1 {
			copy(result.Li, jpegHufftableLumaAc[1:])
			copy(result.Vij, jpegHufftableLumaAc[17:])
		} else {
			copy(result.Li, jpegHufftableLumaDc[1:])
			copy(result.Vij, jpegHufftableLumaDc[17:])
		}
	} else {
		if tc == 1 {
			copy(result.Li, jpegHufftableChromaAc[1:])
			copy(result.Vij, jpegHufftableChromaAc[17:])
		} else {
			copy(result.Li, jpegHufftableChromaDc[1:])
			copy(result.Vij, jpegHufftableChromaDc[17:])
		}
	}

	return result
}

func newScanHeader(nComponents int) jpegScanHeader {
	result := jpegScanHeader{
		SOS:           sos,
		Ns:            uint8(nComponents),
		Ss:            0,
		Se:            63,
		AhAl:          0,
		ScanComponent: make([]jpegScanComponent, nComponents),
		Ls:            uint16(3 + (nComponents * 2) + 3),
	}

	result.ScanComponent[0].Csj = 1
	result.ScanComponent[0].SetTdj(0)
	result.ScanComponent[0].SetTaj(0)

	if nComponents > 1 {
		result.ScanComponent[1].Csj = 2
		result.ScanComponent[1].SetTdj(1)
		result.ScanComponent[1].SetTaj(1)

		result.ScanComponent[2].Csj = 3
		result.ScanComponent[2].SetTdj(1)
		result.ScanComponent[2].SetTaj(1)
	}

	return result
}

func newPackedHeader(i *image.YCbCr, quality int) ([]byte, error) {
	yuvComponent := newYuvComponentSpecs(i.SubsampleRatio)

	result := new(bytes.Buffer)

	//Add SOI
	if err := binary.Write(result, binary.BigEndian, soi); err != nil {
		return nil, err
	}

	//Add AppData
	if err := binary.Write(result, binary.BigEndian, app0); err != nil {
		return nil, err
	} //APP0 marker
	if err := binary.Write(result, binary.BigEndian, uint16(16)); err != nil {
		return nil, err
	} //Length excluding the marker
	if err := binary.Write(result, binary.BigEndian, uint8('J')); err != nil {
		return nil, err
	} //J
	if err := binary.Write(result, binary.BigEndian, uint8('F')); err != nil {
		return nil, err
	} //F
	if err := binary.Write(result, binary.BigEndian, uint8('I')); err != nil {
		return nil, err
	} //I
	if err := binary.Write(result, binary.BigEndian, uint8('F')); err != nil {
		return nil, err
	} //F
	if err := binary.Write(result, binary.BigEndian, uint8(0)); err != nil {
		return nil, err
	} //0
	if err := binary.Write(result, binary.BigEndian, uint8(1)); err != nil {
		return nil, err
	} //Major Version
	if err := binary.Write(result, binary.BigEndian, uint8(1)); err != nil {
		return nil, err
	} //Minor Version
	if err := binary.Write(result, binary.BigEndian, uint8(1)); err != nil {
		return nil, err
	} //Density units 0:no units, 1:pixels per inch, 2: pixels per cm
	if err := binary.Write(result, binary.BigEndian, uint16(72)); err != nil {
		return nil, err
	} //X density
	if err := binary.Write(result, binary.BigEndian, uint16(72)); err != nil {
		return nil, err
	} //Y density
	if err := binary.Write(result, binary.BigEndian, uint8(0)); err != nil {
		return nil, err
	} //Thumbnail width
	if err := binary.Write(result, binary.BigEndian, uint8(0)); err != nil {
		return nil, err
	} //Thumbnail height

	// Regarding Quantization matrices: As per JPEG Spec ISO/IEC 10918-1:1993(E), Pg-19:
	// "applications may specify values which customize picture quality for their particular
	// image characteristics, display devices, and viewing conditions"

	// Normalization of the quality actor
	if quality < 50 {
		quality = 5000 / quality
	} else {
		quality = 200 - (quality * 2)
	}

	quantLuma := newQuantData(lumaSection, quality)
	if err := binary.Write(result, binary.BigEndian, quantLuma); err != nil {
		return nil, err
	}

	quantChroma := newQuantData(chromaSection, quality)
	if err := binary.Write(result, binary.BigEndian, quantChroma); err != nil {
		return nil, err
	}

	frameHeader := newFrameHeader(i)
	if err := binary.Write(result, binary.BigEndian, frameHeader.SOF); err != nil {
		return nil, fmt.Errorf("cannot write frame header: %w", err)
	}
	if err := binary.Write(result, binary.BigEndian, frameHeader.Lf); err != nil {
		return nil, fmt.Errorf("cannot write frame header: %w", err)
	}
	if err := binary.Write(result, binary.BigEndian, frameHeader.P); err != nil {
		return nil, fmt.Errorf("cannot write frame header: %w", err)
	}
	if err := binary.Write(result, binary.BigEndian, frameHeader.Y); err != nil {
		return nil, fmt.Errorf("cannot write frame header: %w", err)
	}
	if err := binary.Write(result, binary.BigEndian, frameHeader.X); err != nil {
		return nil, fmt.Errorf("cannot write frame header: %w", err)
	}
	if err := binary.Write(result, binary.BigEndian, frameHeader.Nf); err != nil {
		return nil, fmt.Errorf("cannot write frame header: %w", err)
	}
	for i := range frameHeader.Component {
		if err := binary.Write(result, binary.BigEndian, frameHeader.Component[i]); err != nil {
			return nil, fmt.Errorf("cannot write frame header: %w", err)
		}
	}

	for i := 0; i < int(yuvComponent.nComponents) && (i <= 1); i++ {
		dcHuffSectionHdr := newHuffSectionHdr(i, 0)
		binary.Write(result, binary.BigEndian, dcHuffSectionHdr.DHT)
		binary.Write(result, binary.BigEndian, dcHuffSectionHdr.Lh)
		binary.Write(result, binary.BigEndian, dcHuffSectionHdr.TcTh)
		for i := range dcHuffSectionHdr.Li {
			binary.Write(result, binary.BigEndian, dcHuffSectionHdr.Li[i])
		}
		for i := range dcHuffSectionHdr.Vij {
			binary.Write(result, binary.BigEndian, dcHuffSectionHdr.Vij[i])
		}

		acHuffSectionHdr := newHuffSectionHdr(i, 1)
		binary.Write(result, binary.BigEndian, acHuffSectionHdr.DHT)
		binary.Write(result, binary.BigEndian, acHuffSectionHdr.Lh)
		binary.Write(result, binary.BigEndian, acHuffSectionHdr.TcTh)
		for i := range acHuffSectionHdr.Li {
			binary.Write(result, binary.BigEndian, acHuffSectionHdr.Li[i])
		}
		for i := range acHuffSectionHdr.Vij {
			binary.Write(result, binary.BigEndian, acHuffSectionHdr.Vij[i])
		}
	}

	// TODO: Add restart interval if not 0

	scanHeader := newScanHeader(int(yuvComponent.nComponents))
	if err := binary.Write(result, binary.BigEndian, scanHeader.SOS); err != nil {
		return nil, fmt.Errorf("cannot write scan header: %w", err)
	}
	if err := binary.Write(result, binary.BigEndian, scanHeader.Ls); err != nil {
		return nil, fmt.Errorf("cannot write scan header: %w", err)
	}
	if err := binary.Write(result, binary.BigEndian, scanHeader.Ns); err != nil {
		return nil, fmt.Errorf("cannot write scan header: %w", err)
	}
	for i := range scanHeader.ScanComponent {
		if err := binary.Write(result, binary.BigEndian, scanHeader.ScanComponent[i]); err != nil {
			return nil, fmt.Errorf("cannot write scan header: %w", err)
		}
	}
	if err := binary.Write(result, binary.BigEndian, scanHeader.Ss); err != nil {
		return nil, fmt.Errorf("cannot write scan header: %w", err)
	}
	if err := binary.Write(result, binary.BigEndian, scanHeader.Se); err != nil {
		return nil, fmt.Errorf("cannot write scan header: %w", err)
	}
	if err := binary.Write(result, binary.BigEndian, scanHeader.AhAl); err != nil {
		return nil, fmt.Errorf("cannot write scan header: %w", err)
	}

	return result.Bytes(), nil
}

func encodeYCbCr(w io.Writer, m *image.YCbCr, o *jpeg.Options) error {
	yuvComponent := newYuvComponentSpecs(m.SubsampleRatio)

	/* Initialize the va driver */
	dev := NewDevice()

	if err := dev.Init(); err != nil {
		return err
	}

	defer dev.Close()

	/* Query for the entrypoints for the JPEGBaseline profile */
	entrypoints, err := dev.GetEntryPoints(C.VAProfileJPEGBaseline)
	if err != nil {
		return err
	}

	if !entrypoints.Has(C.VAEntrypointEncPicture) {
		return errors.New("no hardware encoder available")
	}

	/* Query for the Render Target format supported */
	attrib, err := dev.GetConfigAttributes(C.VAProfileJPEGBaseline, C.VAEntrypointEncPicture, []C.VAConfigAttrib{{_type: C.VAConfigAttribRTFormat}, {_type: C.VAConfigAttribEncJPEG}})
	if err != nil {
		return err
	}

	jpegAttrib := C.VAConfigAttribValEncJPEG{}

	C.setVAConfigAttribValEncJPEGValue(&jpegAttrib, attrib[1].value)
	C.setVAConfigAttribValEncJPEGArithmaticCodingMode(&jpegAttrib, 0)
	C.setVAConfigAttribValEncJPEGProgressiveDctMode(&jpegAttrib, 0)
	C.setVAConfigAttribValEncJPEGNonInterleavedMode(&jpegAttrib, 1)
	C.setVAConfigAttribValEncJPEGDifferentialMode(&jpegAttrib, 0)

	attrib[1].value = C.getVAConfigAttribValEncJPEGValue(&jpegAttrib)

	if (uint(attrib[0].value) & uint(yuvComponent.vaSurfaceFormat)) == 0 {
		return errors.New("format not supported")
	}

	/* Create Config for the profile=VAProfileJPEGBaseline, entrypoint=VAEntrypointEncPicture,
	 * with RT format attribute */
	config, err := dev.CreateConfig(C.VAProfileJPEGBaseline, C.VAEntrypointEncPicture, attrib)
	if err != nil {
		return err
	}

	defer config.Close()

	/* Create Surface for the input picture */
	surfaceAttribute := newVASurfaceAttrib(m.SubsampleRatio)

	surfaces, err := dev.CreateSurfaces(yuvComponent.vaSurfaceFormat, uint(m.Rect.Dx()), uint(m.Rect.Dy()), 1, []C.VASurfaceAttrib{surfaceAttribute})
	if err != nil {
		return err
	}

	defer surfaces.Close()

	surfaces.UploadYVbCr(0, m)

	/* Create Context for the encode pipe*/
	context, err := dev.CreateContext(config, m.Rect.Dx(), m.Rect.Dy(), C.VA_PROGRESSIVE, surfaces)
	if err != nil {
		return err
	}

	defer context.Close()

	/* Create buffer for Encoded data to be stored */
	codedBuffer, err := context.CreateBuffer(C.VAEncCodedBufferType, uint(m.Rect.Dx()*m.Rect.Dy()*3), 1, nil)
	if err != nil {
		return err
	}

	defer codedBuffer.Close()

	/* Create buffer for the picture parameter */
	pictureParameters := newEncPictureParameters(codedBuffer.id, m, o.Quality)

	pictureParamBuffer, err := context.CreateBuffer(C.VAEncPictureParameterBufferType, C.sizeof_VAEncPictureParameterBufferJPEG, 1, unsafe.Pointer(&pictureParameters))
	if err != nil {
		return err
	}

	defer pictureParamBuffer.Close()

	/* Create buffer for Quantization Matrix */
	quantizationParameters := newQMatrix(m)

	quantizationParamBuffer, err := context.CreateBuffer(C.VAQMatrixBufferType, C.sizeof_VAQMatrixBufferJPEG, 1, unsafe.Pointer(&quantizationParameters))
	if err != nil {
		return err
	}

	defer quantizationParamBuffer.Close()

	/* Create buffer for Huffman Tables */
	huffmanTable := newHuffmanTable()

	huffmanTableBuffer, err := context.CreateBuffer(C.VAHuffmanTableBufferType, C.sizeof_VAHuffmanTableBufferJPEGBaseline, 1, unsafe.Pointer(&huffmanTable))
	if err != nil {
		return err
	}

	defer huffmanTableBuffer.Close()

	/* Create buffer for slice parameter */

	sliceParamter := newSliceParameter(yuvComponent.nComponents)

	sliceParameterBuffer, err := context.CreateBuffer(C.VAEncSliceParameterBufferType, C.sizeof_VAEncSliceParameterBufferJPEG, 1, unsafe.Pointer(&sliceParamter))
	if err != nil {
		return err
	}

	defer sliceParameterBuffer.Close()

	/* Create raw buffer for header */
	packedHeaderData, err := newPackedHeader(m, o.Quality)
	if err != nil {
		return err
	}

	packedHeaderParam := C.VAEncPackedHeaderParameterBuffer{
		_type:               C.VAEncPackedHeaderRawData,
		bit_length:          C.uint(len(packedHeaderData) * 8),
		has_emulation_bytes: 0,
	}

	packedHeaderParameterBuffer, err := context.CreateBuffer(C.VAEncPackedHeaderParameterBufferType, C.sizeof_VAEncPackedHeaderParameterBuffer, 1, unsafe.Pointer(&packedHeaderParam))
	if err != nil {
		return err
	}

	defer packedHeaderParameterBuffer.Close()

	packedRawHeaderBuffer, err := context.CreateBuffer(C.VAEncPackedHeaderDataBufferType, uint(len(packedHeaderData)), 1, unsafe.Pointer(&packedHeaderData[0]))
	if err != nil {
		return err
	}

	defer packedRawHeaderBuffer.Close()

	// Begin picture

	if err := context.BeginPicture(surfaces.id[0]); err != nil {
		return err
	}

	// Render picture
	if err := context.RenderPicture([]C.VABufferID{
		pictureParamBuffer.id,
		quantizationParamBuffer.id,
		huffmanTableBuffer.id,
		sliceParameterBuffer.id,
		packedHeaderParameterBuffer.id,
		packedRawHeaderBuffer.id,
	}); err != nil {
		return err
	}

	// End picture
	if err := context.EndPicture(); err != nil {
		return err
	}

	// Write to output

	if err := surfaces.Sync(0); err != nil {
		return err
	}

	// TODO: What to do with the queried status?
	status, err := surfaces.Status(0)
	if err != nil {
		return err
	}

	if status != C.VASurfaceReady {
		return fmt.Errorf("unexpected surface status: %v", status)
	}

	data, err := dev.MapEncodedBuffer(codedBuffer.id)
	if err != nil {
		return err
	}

	defer dev.UnmapBuffer(codedBuffer.id)

	_, err = w.Write(data.Bytes())
	return err

	// return errors.New("pending")
}

func Encode(w io.Writer, m image.Image, o *jpeg.Options) error {
	if o.Quality > 100 {
		o.Quality = 100
	}
	if o.Quality < 1 {
		o.Quality = 1
	}

	switch i := m.(type) {
	case *image.YCbCr:
		return encodeYCbCr(w, i, o)
	default:
		// Fallback to software encoding
		return jpeg.Encode(w, m, o)
	}
}
