package vaapi

/*
#include <va/va.h>
*/
import "C"
import "image"

type Surfaces struct {
	id []C.VASurfaceID
	d  *Device
}

func (s *Surfaces) Close() error {
	if vaStatus := C.vaDestroySurfaces(s.d.d, &s.id[0], C.int(len(s.id))); vaStatus != C.VA_STATUS_SUCCESS {
		return VAError(vaStatus)
	}

	return nil
}

func (s *Surfaces) DeriveImage(id int) (*Image, error) {
	result := &Image{
		d: s.d,
	}

	if vaStatus := C.vaDeriveImage(s.d.d, s.id[id], &result.id); vaStatus != C.VA_STATUS_SUCCESS {
		return nil, VAError(vaStatus)
	}

	return result, nil
}

func (s *Surfaces) UploadYVbCr(id int, i *image.YCbCr) error {
	image, err := s.DeriveImage(id)
	if err != nil {
		return err
	}

	defer image.Close()

	return image.UploadYVbCr(i)
}

func (s *Surfaces) Sync(id int) error {
	if vaStatus := C.vaSyncSurface(s.d.d, s.id[id]); vaStatus != C.VA_STATUS_SUCCESS {
		return VAError(vaStatus)
	}

	return nil
}

func (s *Surfaces) Status(id int) (C.VASurfaceStatus, error) {
	var status C.VASurfaceStatus

	if vaStatus := C.vaQuerySurfaceStatus(s.d.d, s.id[id], &status); vaStatus != C.VA_STATUS_SUCCESS {
		return 0, VAError(vaStatus)
	}

	return status, nil
}
