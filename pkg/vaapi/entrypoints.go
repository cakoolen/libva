package vaapi

/*
#include <va/va.h>
*/
import "C"

type EntryPoints []C.VAEntrypoint

func (e EntryPoints) Has(p C.VAEntrypoint) bool {
	for _, point := range e {
		if point == p {
			return true
		}
	}
	return false
}
