package vaapi

/*
#cgo CFLAGS: -DHAVE_CONFIG_H -I/usr/include/libdrm
#cgo LDFLAGS:  -ldrm -lva-drm -lva

#include "va_display.h"
#include <va/va.h>
*/
import "C"
import (
	"reflect"
	"unsafe"
)

type Device struct {
	d       C.VADisplay
	version struct {
		major C.int
		minor C.int
	}
}

func NewDevice() *Device {
	return &Device{
		d: C.va_open_display(),
	}
}

func (d *Device) Init() error {
	if vaStatus := C.vaInitialize(d.d, &d.version.major, &d.version.minor); vaStatus != C.VA_STATUS_SUCCESS {
		return VAError(vaStatus)
	}
	return nil
}

func (d *Device) Close() error {
	if vaStatus := C.vaTerminate(d.d); vaStatus != C.VA_STATUS_SUCCESS {
		return VAError(vaStatus)
	}

	C.va_close_display(d.d)

	return nil
}

func (d *Device) GetEntryPoints(profile C.VAProfile) (EntryPoints, error) {
	entrypoints := make(EntryPoints, C.vaMaxNumEntrypoints(d.d))

	var numEntryPoints C.int

	if vaStatus := C.vaQueryConfigEntrypoints(d.d, profile, (*C.VAEntrypoint)(unsafe.Pointer(&entrypoints[0])), &numEntryPoints); vaStatus != C.VA_STATUS_SUCCESS {
		return nil, VAError(vaStatus)
	}
	return entrypoints[:numEntryPoints], nil
}

func (d *Device) GetConfigAttributes(profile C.VAProfile, entryPoint C.VAEntrypoint, list []C.VAConfigAttrib) ([]C.VAConfigAttrib, error) {
	result := make([]C.VAConfigAttrib, len(list))
	copy(result, list)

	if vaStatus := C.vaGetConfigAttributes(d.d, profile, entryPoint, (*C.VAConfigAttrib)(unsafe.Pointer(&result[0])), C.int(len(result))); vaStatus != C.VA_STATUS_SUCCESS {
		return nil, VAError(vaStatus)
	}

	return result, nil
}

type Config struct {
	id C.VAConfigID
	d  *Device
}

func (d *Device) CreateConfig(profile C.VAProfile, entryPoint C.VAEntrypoint, list []C.VAConfigAttrib) (*Config, error) {
	result := &Config{
		d: d,
	}

	if vaStatus := C.vaCreateConfig(d.d, profile, entryPoint, (*C.VAConfigAttrib)(unsafe.Pointer(&list[0])), C.int(len(list)), &result.id); vaStatus != C.VA_STATUS_SUCCESS {
		return nil, VAError(vaStatus)
	}

	return result, nil
}

func (c *Config) Close() error {
	if vaStatus := C.vaDestroyConfig(c.d.d, c.id); vaStatus != C.VA_STATUS_SUCCESS {
		return VAError(vaStatus)
	}
	return nil
}

func (d *Device) CreateSurfaces(format, width, height, numSurfaces uint, attributes []C.VASurfaceAttrib) (*Surfaces, error) {
	result := &Surfaces{
		id: make([]C.VASurfaceID, numSurfaces),
		d:  d,
	}

	if vaStatus := C.vaCreateSurfaces(d.d, C.uint(format), C.uint(width), C.uint(height), (*C.VASurfaceID)(unsafe.Pointer(&result.id[0])), C.uint(numSurfaces), (*C.VASurfaceAttrib)(unsafe.Pointer(&attributes[0])), C.uint(len(attributes))); vaStatus != C.VA_STATUS_SUCCESS {
		return nil, VAError(vaStatus)
	}
	return result, nil
}

func (d *Device) MapBuffer(bufferID C.VABufferID, size C.uint) ([]byte, error) {
	var buffer unsafe.Pointer

	if vaStatus := C.vaMapBuffer(d.d, bufferID, &buffer); vaStatus != C.VA_STATUS_SUCCESS {
		return nil, VAError(vaStatus)
	}

	var data []byte

	sh := (*reflect.SliceHeader)(unsafe.Pointer(&data))
	sh.Data = uintptr(buffer)
	sh.Len = int(size)
	sh.Cap = int(size)

	return data, nil
}

func (d *Device) MapEncodedBuffer(bufferID C.VABufferID) (*C.VACodedBufferSegment, error) {
	var buffer unsafe.Pointer

	if vaStatus := C.vaMapBuffer(d.d, bufferID, &buffer); vaStatus != C.VA_STATUS_SUCCESS {
		return nil, VAError(vaStatus)
	}

	return (*C.VACodedBufferSegment)(buffer), nil
}

func (s C.VACodedBufferSegment) Bytes() []byte {
	var data []byte

	sh := (*reflect.SliceHeader)(unsafe.Pointer(&data))
	sh.Data = uintptr(s.buf)
	sh.Len = int(s.size)
	sh.Cap = int(s.size)

	return data
}

func (d *Device) UnmapBuffer(bufferID C.VABufferID) error {
	if vaStatus := C.vaUnmapBuffer(d.d, bufferID); vaStatus != C.VA_STATUS_SUCCESS {
		return VAError(vaStatus)
	}

	return nil
}

func (d *Device) CreateContext(config *Config, width, height, flag int, surfaces *Surfaces) (*Context, error) {
	result := &Context{
		d: d,
	}

	if vaStatus := C.vaCreateContext(d.d, config.id, C.int(width), C.int(height), C.int(flag), &surfaces.id[0], C.int(len(surfaces.id)), &result.id); vaStatus != C.VA_STATUS_SUCCESS {
		return nil, VAError(vaStatus)
	}

	return result, nil
}
