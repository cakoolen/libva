package vaapi_test

import (
	"bytes"
	"fmt"
	"image"
	"image/jpeg"
	"io"
	"os"
	"testing"

	"github.com/google/go-cmp/cmp"

	"gitlab.com/cakoolen/libva/pkg/vaapi"
)

func yCbCrSize(r image.Rectangle, subsampleRatio image.YCbCrSubsampleRatio) (w, h, cw, ch int) {
	w, h = r.Dx(), r.Dy()
	switch subsampleRatio {
	case image.YCbCrSubsampleRatio422:
		cw = (r.Max.X+1)/2 - r.Min.X/2
		ch = h
	case image.YCbCrSubsampleRatio420:
		cw = (r.Max.X+1)/2 - r.Min.X/2
		ch = (r.Max.Y+1)/2 - r.Min.Y/2
	case image.YCbCrSubsampleRatio440:
		cw = w
		ch = (r.Max.Y+1)/2 - r.Min.Y/2
	case image.YCbCrSubsampleRatio411:
		cw = (r.Max.X+3)/4 - r.Min.X/4
		ch = h
	case image.YCbCrSubsampleRatio410:
		cw = (r.Max.X+3)/4 - r.Min.X/4
		ch = (r.Max.Y+1)/2 - r.Min.Y/2
	default:
		// Default to 4:4:4 subsampling.
		cw = w
		ch = h
	}
	return
}

func assertParseI420(tb testing.TB, filename string, width, height int) image.Image {
	d := assertRead(tb, filename)

	i := image.NewYCbCr(image.Rect(0, 0, width, height), image.YCbCrSubsampleRatio420)

	w, h, cw, ch := yCbCrSize(i.Rect, i.SubsampleRatio)

	i0 := w*h + 0*cw*ch
	i1 := w*h + 1*cw*ch
	i2 := w*h + 2*cw*ch

	if len(d) != i2 {
		tb.Fatalf("Unexpected amount of data for %q: %v != %v", filename, len(d), i2)
	}

	i.Y = d[:i0:i0]
	i.Cb = d[i0:i1:i1]
	i.Cr = d[i1:i2:i2]

	return i
}

func assertRead(tb testing.TB, filename string) []byte {
	d, err := os.ReadFile(filename)
	if err != nil {
		tb.Fatalf("Cannot read file %q: %v", filename, err)
	}

	return d
}

func TestEncode(t *testing.T) {
	type args struct {
		m image.Image
		o *jpeg.Options
	}
	tests := map[string]struct {
		args    args
		wantW   []byte
		wantErr bool
	}{
		"i420": {
			args: args{
				m: assertParseI420(t, "testdata/testscreen.yuv", 1920, 1080),
				o: &jpeg.Options{Quality: 90},
			},
			wantW: assertRead(t, "testdata/testscreen.jpg"),
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			w := &bytes.Buffer{}
			if err := vaapi.Encode(w, tt.args.m, tt.args.o); (err != nil) != tt.wantErr {
				t.Errorf("Encode() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if diff := cmp.Diff(w.Bytes(), tt.wantW); diff != "" {
				t.Errorf("Encode()\n%v", diff)
				if err := os.WriteFile("testdata/out.jpg", w.Bytes(), 0644); err != nil {
					t.Errorf("Cannot write output to file: %v", err)
				}
			}
		})
	}
}

type encodeFunc func(io.Writer, image.Image, *jpeg.Options) error

func BenchmarkJPEG(b *testing.B) {
	m := assertParseI420(b, "testdata/testscreen.yuv", 1920, 1080)
	w := new(bytes.Buffer)

	b.ResetTimer()

	for _, encode := range []encodeFunc{jpeg.Encode, vaapi.Encode} {
		b.Run(fmt.Sprintf("%T", encode), func(b *testing.B) {
			for n := 0; n < b.N; n++ {
				w.Reset()
				if err := encode(w, m, &jpeg.Options{Quality: 90}); err != nil {
					b.Fatalf("Cannot encode image: %v", err)
				}
			}
		})
	}

	b.Cleanup(w.Reset)
}
