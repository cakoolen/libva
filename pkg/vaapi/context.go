package vaapi

/*
#include <va/va.h>
*/
import "C"
import "unsafe"

type Context struct {
	id C.VAContextID
	d  *Device
}

func (c *Context) Close() error {
	if vaStatus := C.vaDestroyContext(c.d.d, c.id); vaStatus != C.VA_STATUS_SUCCESS {
		return VAError(vaStatus)
	}

	return nil
}

type Buffer struct {
	id C.VABufferID
	d  *Device
}

func (c *Context) CreateBuffer(bufferType C.VABufferType, size, nElements uint, data unsafe.Pointer) (*Buffer, error) {
	result := &Buffer{
		d: c.d,
	}

	if vaStatus := C.vaCreateBuffer(c.d.d, c.id, bufferType, C.uint(size), C.uint(nElements), data, &result.id); vaStatus != C.VA_STATUS_SUCCESS {
		return nil, VAError(vaStatus)
	}

	return result, nil
}

func (b *Buffer) Close() error {
	if vaStatus := C.vaDestroyBuffer(b.d.d, b.id); vaStatus != C.VA_STATUS_SUCCESS {
		return VAError(vaStatus)
	}

	return nil
}

func (c *Context) BeginPicture(surfaceID C.VASurfaceID) error {
	if vaStatus := C.vaBeginPicture(c.d.d, c.id, surfaceID); vaStatus != C.VA_STATUS_SUCCESS {
		return VAError(vaStatus)
	}

	return nil
}

func (c *Context) RenderPicture(buffers []C.VABufferID) error {
	if vaStatus := C.vaRenderPicture(c.d.d, c.id, &buffers[0], C.int(len(buffers))); vaStatus != C.VA_STATUS_SUCCESS {
		return VAError(vaStatus)
	}

	return nil
}

func (c *Context) EndPicture() error {
	if vaStatus := C.vaEndPicture(c.d.d, c.id); vaStatus != C.VA_STATUS_SUCCESS {
		return VAError(vaStatus)
	}

	return nil
}
